<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Comentario extends Model
{
    protected $table = "tblcomentarios";
    protected $primaryKey = "intcomentarioid";
    public $timestamps = false;

    //RELACIONAMENTOS 
    public function filme(){
        return $this->hasOne(Filme::class, 'intfilmeid', 'intfilmeid');
    }

    public function usuario(){
        return $this->hasOne(Usuario::class, 'intusuarioid', 'intusuarioid');
    }

    //MÉTODOS DE AÇÃO
    public function preencher($dados){
        $this->strcomentario = $dados->strcomentario;
        $this->intfilmeid = $dados->intfilmeid;
        $this->intusuarioid = $dados->intusuarioid;
        $this->dtaregistro = date('Y-m-d H-i-s');
        return $this;

    }

    public function salvar($dados){
        $this->preencher($dados);
        $this->save();
    }
}
