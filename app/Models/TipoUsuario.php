<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TipoUsuario extends Model
{
    protected $table = "tbltipousuario";
    protected $primaryKey = "inttipousuarioid";
    public $timestamps = false;
}
