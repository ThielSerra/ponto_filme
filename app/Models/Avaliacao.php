<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Avaliacao extends Model
{
    protected $table = "tblavaliacoes";
    protected $primaryKey = "intavaliacaoid";
    public $timestamps = false;

    //RELACIONAMENTOS
    public function avaliacoesStatus(){
        return $this->hasOne( AvaliacaoStatus::class, 'intavaliacaostatusid', 'intavaliacaostatusid');
    }

    public function filme(){
        return $this->hasOne( Filme::class, 'intfilmeid', 'intfilmeid');
    }

    //METODOS DE AÇÕES
    public function preencher($dados){
        $this->intfilmeid = $dados->intfilmeid;
        $this->intavaliacaostatusid = $dados->intavaliacaostatusid;
        return $this;
    } 

    public function salvar($dados){
        $this->preencher($dados);
        $this->save();
    }

}
