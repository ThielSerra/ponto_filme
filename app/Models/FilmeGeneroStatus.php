<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class FilmeGeneroStatus extends Model
{
    protected $table = "tblfilmegenerostatus";
    protected $primaryKey = "intfilmegenerostatusid";
    public $timestamps = false;
}
