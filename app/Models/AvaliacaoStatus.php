<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AvaliacaoStatus extends Model
{
    protected $table = "tblavaliacaostatus";
    protected $primaryKey = "intavaliacaostatusid";
    public $timestamps = false;

    public function avaliacao(){

        return $this->belongsTo( Avaliacao::class, 'intavaliacaostatusid',  'intavaliacaostatusid');

    }
    
}
