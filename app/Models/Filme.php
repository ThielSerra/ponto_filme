<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Filme extends Model
{
    protected $table = "tblfilmes";
    protected $primaryKey = "intfilmeid";
    public $timestamps = false;

    //RELACIONAMENTOS
    public function filmeGeneroStatus(){
        return $this->hasOne(FilmeGeneroStatus::class, 'intfilmegenerostatusid', 'intfilmegenerostatusid');
    }

    //ATRIBUTOS COMPUTADOS
    public function GetMediaAvaliacaoFormatadoAttribute(){
        
        $avaliacoes = Avaliacao::where('intfilmeid', $this->intfilmeid)
        ->get();

        if(!$avaliacoes->count()) return 0;
            
        $tamanhoArray = $avaliacoes->count();
        $soma = 0;

        for($cont = 0; $cont < $tamanhoArray; $cont++){

            $soma += $avaliacoes[$cont]->avaliacoesStatus->intvalornota;

        }
        
        $mediaFilme = $soma / $tamanhoArray;

        return number_format($mediaFilme, 1);
        
    }

    //MÉTODOS DE AÇÕES
    public function preencher($dados){
        $this->strtitulo                = $dados->strtitulo;
        $this->strdescricao             = $dados->strdescricao;
        $this->strdiretor               = $dados->strdiretor;
        $this->intfilmegenerostatusid   = $dados->intfilmegenerostatusid;
        return $this;
    }

    public function salvar($dados){
        $this->preencher($dados);
        $this->save();
    }
}
