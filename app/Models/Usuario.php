<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Usuario extends Model
{
    protected $table = 'tblusuarios';
    protected $primaryKey = 'intusuarioid';
    public $timestamps = false;

    //RELACIONAMENTOS
    public function tipoUsuario(){
        return $this->hasOne(TipoUsuario::class, 'inttipousuarioid', 'inttipousuarioid');
    }

    public function genero(){
        return $this->hasOne(TipoUsuario::class, 'intgeneroid', 'intgeneroid');
    }

    //ESCOPOS LOCAIS
    public function scopeUsuarioComum($query){
        return $query->where('inttipousuarioid', 1);
    }

    public function scopeUsuarioAdmin($query){
        return $query->where('inttipousuarioid', 2);
    }

    //MÉTODOS DE AÇÕES
    public function preencher($dados){
        $this->strnome = $dados->strnome;
        $this->stremail = $dados->stremail;
        $this->dtanascimento = $dados->dtanascimento;
        $this->intgeneroid = $dados->intgeneroid;
        $this->inttipousuarioid = $dados->inttipousuarioid ?? 1;
        $this->strsenha = md5($dados->strsenha);
        return $this;
    }

    public function salvar($dados){
        $this->preencher($dados);
        $this->save();
    }

}
