<?php

namespace App\Http\Middleware;

use Closure;
use App\Traits\UsuarioSessao;

class Admin
{
    use UsuarioSessao;

    public function handle($request, Closure $next)
    {

        if(!$this->temPermissaoDeAdministrador()){
            
            return redirect('/sair');

        }

        return $next($request);
    }
}
