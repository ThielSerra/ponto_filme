<?php

namespace App\Http\Middleware;

use Closure;
use App\Traits\UsuarioSessao;

class Usuario
{

    use UsuarioSessao;

    public function handle($request, Closure $next){


        if(!$this->usuarioLogado()){
            
            return redirect('/');

        }

        return $next($request);
       
        
    }

}
