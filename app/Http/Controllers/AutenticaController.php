<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Usuario;

class AutenticaController extends Controller
{
    public function autenticar(Request $request){

        // $query = Usuario::where('stremail', $request->stremail)
        // ->where('strsenha', md5($request->strsenha));
        
        // if($query->usuarioComum()){

        //     $usuarioComum = $query->usuarioComum()
        //     ->first();
        //     dd($usuarioComum );

        // }
        
        // if($query->usuarioAdmin()){

        //     $usuarioAdmin = $query->usuarioAdmin()
        //     ->first();
        //     dd($usuarioAdmin);

        // }

        $usuarioComum = Usuario::where('stremail', $request->stremail)
        ->where('strsenha', md5($request->strsenha))
        ->usuarioComum()
        ->first();

        if($usuarioComum){
            $request->session()->put([
                'usuarioComum'=> $usuarioComum
            ]);
            return redirect('/home');       
        }

        $usuarioAdmin = Usuario::where('stremail', $request->stremail)
        ->where('strsenha', md5($request->strsenha))
        ->usuarioAdmin()
        ->first();

        if($usuarioAdmin){
            $request->session()->put([
                'usuarioAdmin'=> $usuarioAdmin
            ]);
            return redirect('/home');  
        }

        if(!$usuarioComum && !$usuarioAdmin){
            return redirect('/');
        }

    }
}
