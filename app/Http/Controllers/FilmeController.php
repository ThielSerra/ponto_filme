<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Filme;
use App\Models\AvaliacaoStatus;
use App\Models\FilmeGeneroStatus;
use App\Models\Avaliacao;
use App\Models\Usuario;
use App\Models\Comentario;

use App\Traits\UsuarioSessao;

class FilmeController extends Controller
{
    use UsuarioSessao;

    public function index(Request $request){
        
        $usuarioLogado = $this->usuarioLogado();

        $filmes = Filme::all();
        return view('menu.index', compact('filmes','usuarioLogado'));
    }

    public function exibirFilme(Request $request, Avaliacao $media, $intfilmeid){
        
        $filme = Filme::where('intfilmeid', $intfilmeid)->first();
        $comentarios = Comentario::where('intfilmeid', $intfilmeid)->get();
        $usuarioLogado = $this->usuarioLogado();    
        $avaliacaoStatus = AvaliacaoStatus::all();

        $resultMedia = $filme->GetMediaAvaliacaoFormatadoAttribute();

        return view('menu.filme', compact('filme','avaliacaoStatus','usuarioLogado', 'resultMedia', 'comentarios'));
    }

    public function avaliarFilme(Request $request, Avaliacao $avaliacao){
       
        $dados = (object) $request;
        $avaliacao->salvar($dados);

        return redirect('/home/filme/'. $dados->intfilmeid);
    }

    public function exibirFormFilme(Request $request){

        // if($this->temPermissaoDeAdministrador()){
            
            $usuarioLogado = $this->temPermissaoDeAdministrador();
            $generos = FilmeGeneroStatus::all();
            
            return view('menu.formFilme', compact('usuarioLogado', 'generos'));
        // }       
    }

    public function adicionarFilme(Request $request, Filme $filme){

        // if($this->temPermissaoDeAdministrador()){
        
            $dados = $request;
            $filme->salvar($dados);
    
            return redirect('/home');
        // }   
    }

    public function comentarFilme(Request $request){
        
        $comentario = new Comentario();
        $dados = $request;
        $comentario->salvar($dados);

        return redirect('/home/filme/'. $dados->intfilmeid);

    }
    
}
