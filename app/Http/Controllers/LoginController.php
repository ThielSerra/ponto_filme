<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class LoginController extends Controller
{
    public function index(){
        
        return view('login.index');
    }

    public function sair(Request $request){
        $usuarioComum = $request->session()->forget('usuarioComum');
        $usuarioAdmin = $request->session()->forget('usuarioAdmin');
        return redirect('/');
    }
}
