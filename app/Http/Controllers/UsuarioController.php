<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Usuario;
use App\Models\Genero;
use App\Models\TipoUsuario;

class UsuarioController extends Controller
{
    public function exibirFormCadastro(){
        $generos = Genero::all();
        $tipoUsuarios = TipoUsuario::all();
        return view('usuario.formCadastro', compact('generos','tipoUsuarios'));
    }

    public function cadastrar(Request $request){

        $dados = (object) $request;
        $usuario = new Usuario;
        $usuario->salvar($dados);
        
        return view('login.index');
    }
}
