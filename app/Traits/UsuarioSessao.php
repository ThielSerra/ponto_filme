<?php

namespace App\Traits;

use Illuminate\Http\Request;

trait UsuarioSessao
{
    public static function usuarioLogado(){
        
        return session('usuarioComum') ?? session('usuarioAdmin');       
        
    }

    public static function temPermissaoDeAdministrador(){

        return session('usuarioAdmin');
        
    }
}