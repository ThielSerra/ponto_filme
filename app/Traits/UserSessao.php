<?php

namespace App\Traits;

use Illuminate\Http\Request;

trait UserSessao
{
    public static function usuarioLogado(Request $request){
        
        $usuarioAdmin = $request->session()->get('usuarioAdmin');
        $usuarioComum = $request->session()->get('usuarioComum');
        
        $usuarioLogado = $usuarioAdmin ?? $usuarioComum;      
        
        return $usuarioLogado;
        
    }
}