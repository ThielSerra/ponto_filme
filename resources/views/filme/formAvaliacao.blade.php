<div>
    <form action="{{url('/filme/avaliacao')}}" method="POST">
        @csrf
        <input type="hidden" name="intfilmeid" value="{{$filme->intfilmeid}}">
        
        @foreach($avaliacaoStatus as $status)
            <input type="radio" name="intavaliacaostatusid" value="{{$status->intavaliacaostatusid}}"> {{$status->stravaliacaostatus}}
        @endforeach

        <br><br>
        <input class="btn btn-success" type="submit" value="Avaliar">
    </form>
</div>