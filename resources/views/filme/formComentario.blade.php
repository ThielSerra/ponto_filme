<div class="form-group">
    Comentário
    <form action="{{url('/filme/comentario')}}" method="POST">
        @csrf
        <input type="hidden" name="intfilmeid" value="{{$filme->intfilmeid}}">
        <input type="hidden" name="intusuarioid" value="{{$usuarioLogado->intusuarioid}}">
        <textarea name="strcomentario" rows="2" class="form-control"></textarea><br>
        <input class="btn btn-success" type="submit" value="Publicar">
    </form>
</div>