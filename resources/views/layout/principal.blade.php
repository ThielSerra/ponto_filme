<!DOCTYPE html>
<html lang="pt-br">
    <head>
        <meta charset="UTF-8">
        <title>PontoFilme</title>

        <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">

    </head>
    <body>
        <div class="container">
            <nav class="navbar navbar-expand-lg navbar-light bg-light">
                <h1>Ponto Filme</h1>
            </nav>
            @if(session()->has('usuarioComum') or session()->has('usuarioAdmin'))
            <b> {{$usuarioLogado->strnome}} </b> -
                @if(session()->has('usuarioAdmin'))
                    <a type="button" href="{{url('/filme/form')}}">Adicionar</a>
                @endif
                <a type="button" href="{{url('/sair')}}">Sair</a>

            @endif
        </div>
        <hr>

        <div class="container">
            @yield('conteudo')
        </div>

        <div>
            <hr>
            <h6 style="text-align:center">
            Autor: Thiel Serra <br>
            Contato: thiel.serra@gmail.com <br>
            &copy;copyright 2018
            </h6>
        </div> 
    </body>
</html>