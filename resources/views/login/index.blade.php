@extends('layout.principal')
@section('conteudo')
    <div>
        <form action="{{url('/autenticar')}}" method="POST">
            @csrf
            <div class="container">
                
                <div class="form-row">
                    <div class="form-group col-md-4">
                        <label>E-mail:</label>
                        <input type="email" class="form-control"  name="stremail" placeholder="Informe seu email">
                    </div>
                </div>

                <div class="form-row">
                    <div class="form-group col-md-4">
                        <label>Senha:</label>
                        <input type="password" class="form-control"  name="strsenha" placeholder="Informe uma senha">
                    </div>
                </div>

                <div class="form-row">
                    <div class="form-group col-md-5">
                        <input type="submit" class="btn btn-success" value="Entrar"> 
                        &nbsp Não possui cadastro?  <a  href="{{url('/cadastro')}}"> Clique Aqui! </a>               
                    </div>
                    
                </div>


                

            </div>
        </form>  
    </div>
@stop