@extends('layout.principal')
@section('conteudo')
    <div>
        <a  class="btn btn-outline-info btn-sm" href="{{url('/home')}}">Voltar</a> 
        
        <br><br>

        <h4> {{$filme->strtitulo}} </h4>

        Sinopse:<br>   
        <p style="text-align:justify">{{$filme->strdescricao}}</p>

        Avaliação:  {{$resultMedia}}
        <br>
        
        @include('filme.formAvaliacao')

        <hr>

        @if(count($comentarios) > 0)

            @if(count($comentarios) == 1)
                <h3> {{$comentarios->count()}} Comentário</h3>
            @else
                <h3> {{$comentarios->count()}} Comentários</h3>
            @endif

            <ul>
                @foreach($comentarios as $comentario)
                    <li> <b>{{$comentario->intusuarioid}}</b> disse:
                        <ul>
                            {{$comentario->dtaregistro}}
                           <li>{{$comentario->strcomentario}}</li>                    
                        </ul>
                    </li>
                    <br>
                @endforeach
            </ul>
        @endif
            
        @include('filme.formComentario')
    </div>
@stop