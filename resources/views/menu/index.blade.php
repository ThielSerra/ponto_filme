@extends('layout.principal')
@section('conteudo')
    <div>

        <ul>
            @foreach($filmes as $filme)
            <li>
                <a href="{{url('/home/filme/'. $filme->intfilmeid )}}"> {{$filme->strtitulo}} </a>
                
                <ul>
                    <li> <b>Sinopse:</b> <p style="text-align:justify">{{$filme->strdescricao}}</p> </li>
                
                </ul>

            </li> 
            <br>
            @endforeach 
        </ul>
    </div>
@stop