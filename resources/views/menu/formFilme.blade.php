@extends('layout.principal')
@section('conteudo')
    <div>
        <form action="{{url('/filme/add')}}" method="POST">
            @csrf
            <div class="form-group">
                <label>Nome:</label>
                <input type="text" name="strtitulo" class="form-control" placeholder="Informe o nome do filme">
            </div>
            <div class="form-group">
                <label>Sinopse:</label>
                <textarea type="text" name="strdescricao" class="form-control" rows="4" placeholder="Descreva o filme"></textarea>
            </div>
            <div class="form-group">
                <label>Diretor:</label>
                <input type="text" name="strdiretor" class="form-control" placeholder="Informe o nome do diretor">
            </div>

            <div class="form-group">
                <label>Gênero:</label>
                <select name="intfilmegenerostatusid" class="form-control">
                    <option>  </option>
                    @foreach($generos as $genero)
                        <option value="{{$genero->intfilmegenerostatusid}}">{{$genero->strfilmegenerostatus}}</option>
                    @endforeach
                </select>
            </div>

            <button type="submit" class="btn btn-success">Adicionar</button>
            <a  class="btn btn-danger" href="{{url('/home')}}">Cancelar</a>
            
        </form>
    </div>
@stop