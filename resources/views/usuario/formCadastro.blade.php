@extends('layout.principal')
@section('conteudo')
    <div>
        <h4>Formulário</h4>
        
        <br>

        <form action="{{url('/cadastro')}}" method="POST">
            @csrf
            <div class="container">
                
                <div class="form-row">
                    <div class="form-group col-md-9">
                        <label>Nome:</label>
                        <input type="text" class="form-control"  name="strnome" placeholder="Informe seu nome">
                    </div>
                    <div class="form-group col-md-3">
                        <label>Data de nascimento:</label>
                        <input type="date" class="form-control" name="dtanascimento">
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-group col-md-6">
                        <label>Gênero:</label>
                        <select name="intgeneroid" class="form-control">
                            <option> -- Selecione -- </option>
                            @foreach($generos as $genero)
                                <option value="{{$genero->intgeneroid}}">
                                    {{$genero->strgenero}}
                                </option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group col-md-6">
                        <label>Perfil:</label>
                        <select name="inttipousuarioid" class="form-control">
                            <option> -- Selecione -- </option>
                            @foreach($tipoUsuarios as $tipo)
                                <option value="{{$tipo->inttipousuarioid}}">
                                    {{$tipo->strtipousuario}}
                                </option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-group col-md-12">
                        <label>E-mail:</label>
                        <input type="email" class="form-control"  name="stremail" placeholder="Informe seu email">
                    </div>
                </div>

                <div class="form-row">
                    <div class="form-group col-md-6">
                        <label>Senha:</label>
                        <input type="password" class="form-control"  name="strsenha" placeholder="Informe uma senha">
                    </div>
                    <div class="form-group col-md-6">
                        <label>Confirme senha:</label>
                        <input type="password" class="form-control"  name="strconfirmesenha" placeholder="Confirme sua senha">
                    </div>
                </div>
                
                <button type="submit" class="btn btn-success">Cadastrar-se</button>
                <a class="btn btn-danger" href="{{url('/')}}">Cancelar</a>
            </div>
           
        </form>
    </div>
@stop