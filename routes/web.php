<?php

Route::GET('/', 'LoginController@index');

Route::POST('/autenticar', 'AutenticaController@autenticar');

Route::GET('/cadastro', 'UsuarioController@exibirFormCadastro');
Route::POST('/cadastro', 'UsuarioController@cadastrar');

Route::middleware(['usuario'])->group(function () {


    Route::GET('/home', 'FilmeController@index');
    Route::GET('/home/filme/{intfilmeid}', 'FilmeController@exibirFilme');
    
    Route::POST('/filme/avaliacao', 'FilmeController@avaliarFilme');
    Route::POST('/filme/comentario', 'FilmeController@comentarFilme');
    
    Route::GET('/sair', 'LoginController@sair');

});

Route::middleware(['admin'])->group(function () {
    Route::GET('/filme/form', 'FilmeController@exibirFormFilme');
    Route::POST('/filme/add', 'FilmeController@adicionarFilme');
});